"use strict";

let json = `
{
  "name": 6,
  "city": "Villeurbanne",
  "trainers": [
    {"name": "Mathieu", "surname": "Morel"},
    {"name": "Jean", "surname": "Demel"}
  ],
  "ended": false
}
`;

let promo = JSON.parse(json);
json = JSON.stringify(promo);
console.log(json);

let ended = "";

promo.ended ? ended = "formation terminée" : ended = "formation en cours";

// if (promo.ended) {
//   ended = "formation terminée"
// } else {
//   ended = "formation en cours";
// }

let firstLine = `Promo ${promo.name} de ${promo.city}, ${ended}.\n`;

let nextLines = 'Formateurs :\n';

promo.trainers.forEach(trainer => {
  nextLines += `  - ${trainer.name}\n`;
});

let sentence = firstLine + nextLines;